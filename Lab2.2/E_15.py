class MyDirectory:

    def __init__(self):
        self.directory = []

    def create_record(self, name, address='not defined', phone=0, email='something@something.com'):
        self.directory.append([name, address, phone, email])

    def search_info(self, info):
        for record in self.directory:
            for i in [0, 1, 2, 3]:
                if record[i] == info:
                    return record
        return 'The record is not there'

    def save_records(self):
        file = open('MyDirectory', 'W')
        for record in self.directory:
            for i in range(4):
                file.write(record[i] + '\n')
            file.write('\n')
        file.close()


# MyDirectory.create_record('', 'Corey', 'Malta', 3310, 'hola@prueba.com')
emp_1 = MyDirectory.create_record('0', 'h', 'r', '4', 'someting@something.com')
