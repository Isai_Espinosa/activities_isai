class UsersDirectory:  # Heavily based on exercise 15 of lab 2
    def __init__(self):
        self.directory = []

    def add_record(self, name, email='user@something.com', age=0, country='Not defined'):
        """ Adds a new entry to the directory. """
        self.directory.append([name, email, age, country])

    def delete_record(self, record_number):
        if record_number < len(self.directory):
            self.directory.pop(record_number)
        else:
            print('Given record does not exists')

    def search(self, info):
        """ Search provided info in all the directory (mail and age) and returns first record that contains it. """
        for record in self.directory:
            for i in [1, 2]:  # Indexes of email and age
                if record[i] == info:
                    return record
        return 'Not found in directory'

    def get_record(self, record_number):
        """ Returns the record given a record number. """
        return self.directory[record_number]

    def save_to_file_formatted(self, filename):
        """ Saves directory in a formatted way into a text file. """
        file = open(filename, 'w')
        for i, record in enumerate(self.directory):
            file.write(f'Record {i}\n')
            file.write(f'Name: {record[0]}\n')
            file.write(f'Email: {record[1]}\n')
            file.write(f'Age: {record[2]}\n')
            file.write(f'Country: {record[3]}\n\n')
        file.close()

    def save_to_file(self, filename):
        """ Saves directory in a text file. """
        file = open(filename, 'w')
        for record in self.directory:
            for i in range(4):
                file.write(record[i] + '\n')
            file.write('\n')
        file.close()

    def load_from_formatted_file(self, filename):
        """ Loads directory data from a formatted text file. """
        file = open(filename, 'r')
        data = file.readline()
        while data != '':
            end_pos = data.find('\n')
            record = int(data[7:end_pos])
            data = file.readline()
            end_pos = data.find('\n')
            name = data[6:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            address = data[9:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            phone = data[7:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            email = data[7:end_pos]
            file.readline()
            self.directory[record] = [name, address, phone, email]
            data = file.readline()
        file.close()

    def load_from_file(self, filename):
        """ Loads directory data from text file. """
        file = open(filename, 'r')
        data = file.read().split('\n')
        record = 0
        i = 0
        while i < len(data) and data[i] != '':
            self.directory[record] = data[i:i + 4]
            record += 1
            i += 5
        file.close()

    def print_directory(self):
        """ Prints current directory. """
        print('Current directory:')
        for record in self.directory:
            print(str(record))
