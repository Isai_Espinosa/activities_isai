import csv
import numpy as np


class SortMethod:

    def __init__(self):
        pass

    def set_input_data(self, filename):
        """Opens the file and gets the data"""
        with open(filename) as file:
            reader = csv.DictReader(file)
            data = []
            for row in reader:
                data.append(int((row['column1'])))
            return np.array(data)

    def set_output_data(self, filename):
        """Creates a new file to store the sorted data"""
        a = 0
        with open(filename, 'w', newline='') as f:
            fieldnames = ['new_column1']
            thewriter = csv.DictWriter(f, fieldnames=fieldnames)

            thewriter.writeheader()

            for row in result:
                thewriter.writerow({'new_column1': result[a]})
                a += 1


def merge_sort(array):
    """Divides the data in halves"""
    if len(array) <= 1:
        return array

    midpoint = int(len(array) // 2)

    left, right = merge_sort(array[:midpoint]), merge_sort(array[midpoint:])

    return merge(left, right)


def merge(left, right):
    """Sorts the data"""
    result_data = []
    left_pointer = right_pointer = 0

    while left_pointer < len(left) and right_pointer < len(right):

        if left[left_pointer] < right[right_pointer]:

            result_data.append(left[left_pointer])
            left_pointer += 1

        else:

            result_data.append(right[right_pointer])
            right_pointer += 1

    result_data.extend(left[left_pointer:])
    result_data.extend(right[right_pointer:])

    return result_data


data_complete = SortMethod.set_input_data('', filename='mycsv.csv')
print(data_complete)

result = merge_sort(data_complete)
print(result)

SortMethod.set_output_data('', filename='mynewcsv.csv')
