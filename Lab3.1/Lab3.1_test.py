import unittest
import E_17
import E_19


class TestCalc(unittest.TestCase):

    def test_fact(self):
        """It checks that the factorial is correct"""
        self.assertEqual(E_17.fact(5), 120)

    def test_fact2(self):
        """It checks that the factorial of both o and 1 is o"""
        self.assertGreater(E_17.fact(0), 0)
        self.assertGreater(E_17.fact(1), 0)


class TestFile(unittest.TestCase):

    def test_file(self):
        """It should pass if they are the same"""
        self.assertEqual(E_19.file('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                      '/Lab3.1/File_T1.txt'), True)

    def test_file2(self):
        """It should if they are different"""
        self.assertEqual(E_19.file('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                      '/Lab3.1/File_T2.txt'), False)

    def test_file3(self):
        """It should pass if they are the same"""
        self.assertEqual(E_19.file2('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                       '/Lab3.1/File_T1.txt'), True)

    def test_file4(self):
        """It should fail if they are different"""
        self.assertEqual(E_19.file2('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                       '/Lab3.1/File_T2.txt'), True)


if __name__ == '__main__':
    unittest.main()
