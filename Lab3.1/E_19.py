import filecmp


def file(x: object, y: object):
    """Returns the comparison of the names recieved and it should be true"""
    return filecmp.cmp(x, y, shallow=True)


def file2(x: object, y: object):
    """Returns the comparison of the names recieved and it should be false"""
    return filecmp.cmp(x, y, shallow=False)
