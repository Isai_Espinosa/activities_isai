import unittest
import E_17


class TestCalc(unittest.TestCase):

    def test_fact(self):
        """Test the factorial 0f 5"""
        self.assertEqual(E_17.fact(5), 120)

    def test_fact2(self):
        """Test that the factorial of both 0 and 1 is 0"""
        self.assertGreater(E_17.fact(0), 0)
        self.assertGreater(E_17.fact(1), 0)


if __name__ == '__main__':
    unittest.main()