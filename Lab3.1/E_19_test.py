import unittest
import E_19


class TestCalc(unittest.TestCase):

    def test_file(self):
        """It should pass if they are the same"""
        self.assertEqual(E_19.file('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                      '/Lab3.1/File_T1.txt'), True)

    def test_file2(self):
        """It should if they are different"""
        self.assertEqual(E_19.file('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                      '/Lab3.1/File_T2.txt'), False)

    def test_file3(self):
        """It should pass if they are the same"""
        self.assertEqual(E_19.file2('/home/carlos/PycharmProjects/Lab3.1/File_T2.txt', '/home/carlos/PycharmProjects'
                                                                                       '/Lab3.1/File_T2.txt'), True)

    def test_file4(self):
        """It should fail if they are different"""
        self.assertEqual(E_19.file2('/home/carlos/PycharmProjects/Lab3.1/File_T1.txt', '/home/carlos/PycharmProjects'
                                                                                       '/Lab3.1/File_T2.txt'), True)


if __name__ == '__main__':
    unittest.main()
