import unittest
import E_17

class TestCalc(unittest.TestCase):

    def test_add(self):
        self.assertEqual(E_17.add(10, 5), 15)
        self.assertEqual(E_17.add(-1, 1), 0)
        self.assertEqual(E_17.add(-1, -1), -2)

    def test_substract(self):
        self.assertEqual(E_17.substract(10, 5), 5)
        self.assertEqual(E_17.substract(-1, 1), -2)
        self.assertEqual(E_17.substract(-1, -1), 0)

    def test_multiply(self):
        self.assertEqual(E_17.multiply(10, 5), 50)
        self.assertEqual(E_17.multiply(-1, 1), -1)
        self.assertEqual(E_17.multiply(-1, -1), 1)

    def test_divide(self):
        self.assertEqual(E_17.divide(10, 5), 2)
        self.assertEqual(E_17.divide(-1, 1), -1)
        self.assertEqual(E_17.divide(-1, -1), 1)
        self.assertEqual(E_17.divide(5, 2), 2.5)

        with self.assertRaises(ValueError):
            E_17.divide(10, 0)


if __name__ == '__main__':
    unittest.main()