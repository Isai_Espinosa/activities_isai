import math
import filecmp


def fact(x):
    return math.factorial(x)


def file(x: object, y: object):
    return filecmp.cmp(x, y, shallow=True)
