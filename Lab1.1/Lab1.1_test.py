import unittest
import E_8

data = [1, 2, 3, 4, 5, 6]


class TestLab1(unittest.TestCase):
    """The class test to check the quartile results"""

    def test_quartile(self):
        self.assertEqual(E_8.quartile_func(data, .10), 1.005)

    def test_quartile2(self):
        self.assertEqual(E_8.quartile_func(data, .50), 1.025)

    def test_quartile3(self):
        self.assertEqual(E_8.quartile_func(data, .25), 1.0125)

    def test_quartile4(self):
        self.assertGreater(E_8.quartile_func(data, .10), 1)


if __name__ == '__main__':
    unittest.main()
