import random

guessesTaken = 0
number = random.randint(1, 9)
# print(number)

guess = int(input("Enter the number you think is the random: "))

while guess != number:
    """Makes a loop while the number is not correct"""
    if guess < number:
        print("Your guess is too low.")

    if guess > number:
        print("Your guess is too high")

    print("Try again")
    guess = input()
    guess = int(guess)

    guessesTaken = guessesTaken + 1

    if guess == number:
        """When the number is correct it breaks the loop"""
        print("Your number of tries where: ", guessesTaken)
        print("Good job")
        break

    if guess == number:
        """When the number is correct it also prints the number of guesses"""
        guessesTaken = str(guessesTaken)
        print("Good job")
