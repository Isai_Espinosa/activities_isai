class RomanNumber:
    def int_to_roman(self, num):
        """This method converts an integer to a roman formatted number"""

        val = [1000000, 900000, 500000, 400000, 100000, 90000, 50000, 40000, 10000, 9000, 5000, 4000, 1000, 900, 500,
               400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        sym = ["-M-", "-CM-", "-D-", "-CD-", "-C-", "-XC-", "-L-", "-XL-", "-X-", "-IX-", "-V-", "-IV-", "M", "CM", "D",
               "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
        roman_num = ''
        i = 0
        while num > 0:
            for _ in range(num // val[i]):
                roman_num += sym[i]
                num -= val[i]
            i += 1
        return roman_num


print(RomanNumber().int_to_roman(int(input("Insert the decimal number: "))))
print(RomanNumber().int_to_roman(int(input("Insert the decimal number: "))))
