from statistics import mean
import statistics
import numpy as np

sample = [1, 2, 3, 4, 5, 6]

"""Give the average of the data"""
avg = mean(sample)
print("Average is: ", avg)

"""Gives the standard deviation of the data"""
dev = statistics.stdev(sample)
print("Standard deviation is: ", dev)

"""GIves the median of the data"""
med = statistics.median(sample)
print("The median is: ", med)


def quartile():
    """This function gives the quartile 50 of the data"""
    return print("Q10 quartile is: ", np.percentile(sample, .10)), print("Q50 quartile is: ",
                                                                         np.percentile(sample, .50)), print(
        "Q25 quartile is: ", np.percentile(sample, .25))


def percentile():
    """This function gives the percentile 25 of the data"""
    return print("P50 percentile is: ", np.percentile(sample, 50)), print("P25 percentile is: ",
                                                                          np.percentile(sample, 25)), print(
        "P10 percentile is: ", np.percentile(sample, 10))


def quartile_func(data, percent):
    """This function is for the test of the quartile function"""
    return np.percentile(data, percent)


quartile()
percentile()
